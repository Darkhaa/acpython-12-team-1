from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from family_tree import serializers
from rest_framework.views import APIView
from family_tree.models import FamilyTree


# Create your views here.
class FamilyTreePersonalInfoView(APIView):
    def get(self, request, *args, **kwargs):
        try:
            user_data = FamilyTree.objects.get(user_creator_id=kwargs['user_id'])
            serializer = serializers.FamilyTreePersonalInfoSerializer(user_data)
            return Response(serializer.data)
        except FamilyTree.DoesNotExist:
            raise NotFound("Not Found")

    def patch(self, request, *args, **kwargs):
        try:
            user_data = FamilyTree.objects.get(user_creator_id=kwargs['user_id'])
            serializer = serializers.FamilyTreePersonalInfoSerializer(user_data, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=200)
            return Response(serializer.errors, status=400)
        except FamilyTree.DoesNotExist:
            raise NotFound("Not found")
