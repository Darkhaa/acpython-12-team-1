from django.contrib.auth import get_user_model
from django.db import models
import uuid
# Create your models here.


class FamilyTree(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    first_name = models.CharField(max_length=50, blank=True, null=True)
    last_name = models.CharField(max_length=50, blank=True, null=True)
    birth_date = models.DateField(blank=True, null=True)
    die_date = models.DateField(null=True)
    gender = models.CharField(max_length=20, blank=True, null=True)
    biography = models.TextField(blank=True, null=True)
    avatar = models.ImageField(upload_to='avatars', verbose_name='avatar', null=True,
                               blank=True, default='avatar/default.jpg')
    ancestor_id = models.ForeignKey("self", null=True, blank=True, on_delete=models.PROTECT)
    user_creator_id = models.ForeignKey(get_user_model(), null=True, on_delete=models.PROTECT)
