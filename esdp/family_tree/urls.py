from django.urls import path
from family_tree import views


urlpatterns = [
    path('deep_data_profile_from_family_tree/<uuid:user_id>', views.FamilyTreePersonalInfoView.as_view(),
         name='deep_data_profile_from_family_tree'),
]