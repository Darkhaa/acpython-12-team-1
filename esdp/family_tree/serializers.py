from rest_framework import serializers
from family_tree.models import FamilyTree


class FamilyTreePersonalInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = FamilyTree
        fields = '__all__'
        read_only_fields = ['id', ]
