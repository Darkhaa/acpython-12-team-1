#!/bin/sh


python3 manage.py collectstatic --no-input
python3 manage.py migrate --no-input
python3 manage.py loaddata fixtures/main_tree.json
gunicorn core.wsgi -b 0.0.0.0:8000