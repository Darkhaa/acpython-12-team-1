from django.urls import path
from rest_framework import routers

from main_tree import views

router = routers.DefaultRouter()
router.register(r'zhuzs', views.ZhuzViewSet)
router.register(r'clans', views.ClanViewSet)
router.register(r'houses', views.HouseViewSet)
router.register(r'shejire', views.ShejireViewSet)
router.register(r'ancestor', views.AncestorViewSet)

urlpatterns = [
    path('get_clan_by_id/<int:id>', views.ClanView.as_view(), name='get_clan_by_id'),
    path('clans/filter/<str:zhuz_referrence>/', views.FilteredClanListView.as_view(), name='clan_list_with_referrence'),
    path('houses/filter/<str:clan_referrence>/', views.FilteredHouseListView.as_view(),
         name='house_list_with_referrence'),
    path('get_ancestor_by_id/<int:id>', views.AncestorView.as_view(), name='get_ancestor_by_id'),
    path('get_ancestor_by_ancestor_id/<int:id>', views.AncestorToAncestorView.as_view(),
         name='get_ancestor_by_ancestor_id'),
    path('get_zhuz_by_id/<int:id>', views.ZhuzView.as_view(), name='get_zhuz_by_id'),
    path('get_house_by_id/<int:id>', views.HouseView.as_view(), name='get_house_by_id'),
    path('get_single_clan_by_id/<int:id>', views.ClanSingleView.as_view(), name='get_single_clan_by_id'),
]
