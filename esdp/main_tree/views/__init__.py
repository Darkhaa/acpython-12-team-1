from rest_framework import viewsets
from rest_framework.response import Response
from main_tree.models import Zhuz, Clan, Ancestor, House
from rest_framework.views import APIView
from main_tree import serializers
from accounts.models import Shejire
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated, IsAdminUser, AllowAny


class ZhuzViewSet(viewsets.ModelViewSet):
    queryset = Zhuz.objects.all()
    serializer_class = serializers.ZhuzSerializer


class ClanViewSet(viewsets.ModelViewSet):
    queryset = Clan.objects.all()
    serializer_class = serializers.AncestorSerializer


class HouseViewSet(viewsets.ModelViewSet):
    queryset = House.objects.all()
    serializer_class = serializers.HouseSerializer


class AncestorViewSet(viewsets.ModelViewSet):
    queryset = Ancestor.objects.all()
    serializer_class = serializers.AncestorSerializer


class ClanView(APIView):
    def get(self, request, *args, **kwargs):
        queryset = Clan.objects.filter(zhuz_referrence__id=kwargs['id'])
        serializer = serializers.ClanSerializer(queryset, many=True)
        return Response(serializer.data)


class AncestorView(APIView):
    def get(self, request, *args, **kwargs):
        # Фильтруем объекты Ancestor по идентификатору дома
        queryset = Ancestor.objects.filter(house_referrence__id=kwargs['id'])
        serializer = serializers.AncestorSerializer(queryset, many=True)
        return Response(serializer.data)


class AncestorToAncestorView(APIView):
    def get(self, request, *args, **kwargs):
        # Фильтруем объекты Ancestor по идентификатору предка
        queryset = Ancestor.objects.filter(ancestor_referrence__id=kwargs['id'])
        serializer = serializers.AncestorSerializer(queryset, many=True)
        return Response(serializer.data)

class ClanSingleView(APIView):
    def get(self, request, *args, **kwargs):
        queryset = Clan.objects.filter(pk=kwargs['id'])
        serializer = serializers.ClanSerializer(queryset, many=True)
        return Response(serializer.data)


class ZhuzView(APIView):
    def get(self, request, *args, **kwargs):
        queryset = Zhuz.objects.filter(pk=kwargs['id'])
        serializer = serializers.ZhuzSerializer(queryset, many=True)
        return Response(serializer.data)


class HouseView(APIView):
    def get(self, request, *args, **kwargs):
        queryset = House.objects.filter(clan_referrence__id=kwargs['id'])
        serializer = serializers.HouseSerializer(queryset, many=True)
        return Response(serializer.data)


class FilteredClanListView(APIView):
    def get(self, request, zhuz_referrence):
        all_clans = Clan.objects.filter(zhuz_referrence__id=zhuz_referrence)
        serializer = serializers.ClanSerializer(all_clans, many=True)
        return Response(serializer.data)


class FilteredHouseListView(APIView):
    def get(self, request, clan_referrence):
        all_houses = House.objects.filter(clan_referrence__id=clan_referrence)
        serializer = serializers.HouseSerializer(all_houses, many=True)
        return Response(serializer.data)


class ShejireViewSet(viewsets.ModelViewSet):
    queryset = Shejire.objects.all()
    serializer_class = serializers.ShejireSerializer
    permission_classes = [IsAuthenticated]
