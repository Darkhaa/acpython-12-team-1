from rest_framework import serializers
from main_tree.models import Zhuz, Clan, Ancestor, House
from accounts.models import Shejire


class ZhuzSerializer(serializers.ModelSerializer):
    class Meta:
        model = Zhuz
        fields = ['id', 'name']


class ClanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clan
        fields = ['id', 'name', 'zhuz_referrence']
        read_only_fields = ['zhuz_referrence']


class HouseSerializer(serializers.ModelSerializer):
    class Meta:
        model = House
        fields = '__all__'


class AncestorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ancestor
        fields = '__all__'


class ShejireSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shejire
        fields = '__all__'
