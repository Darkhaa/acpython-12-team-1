from django.contrib import admin
from main_tree.models import Zhuz, Clan, House, Ancestor

admin.site.register(Zhuz)
admin.site.register(Clan)
admin.site.register(House)
admin.site.register(Ancestor)
