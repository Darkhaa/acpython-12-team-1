from django.db import models


class Zhuz(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)

    def __str__(self):
        return self.name


class Clan(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    zhuz_referrence = models.ForeignKey(Zhuz, on_delete=models.PROTECT, null=False, blank=False)

    def __str__(self):
        return self.name


class House(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    clan_referrence = models.ForeignKey(Clan, on_delete=models.PROTECT, null=False, blank=False)
    is_approved = models.BooleanField(default=None, null=True, blank=True)
    user = models.ForeignKey('accounts.User', on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return self.name


class Ancestor(models.Model):
    name = models.CharField(max_length=50, null=False, blank=False)
    house_referrence = models.ForeignKey(House, on_delete=models.PROTECT, null=True)
    ancestor_referrence = models.ForeignKey('self', on_delete=models.PROTECT, null=True)
    is_approved = models.BooleanField(default=None, null=True, blank=True)
    user = models.ForeignKey('accounts.User', on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return self.name
