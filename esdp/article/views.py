from django.db.models import Q
from drf_spectacular.contrib import rest_framework
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser
from .models import Post, Tag, Photo, Comments
from .serializers import PostSerializer, TagSerializer, PhotoPostSerializer, CommentPostSerializer
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated, IsAdminUser, AllowAny


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    def get_permissions(self):
        if self.action in ['list', 'retrieve']:
            permission_classes = [AllowAny]
        elif self.action in ['update', 'partial_update', 'destroy', 'create']:
            permission_classes = [IsAdminUser]
        else:
            permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]

    def list(self, request, *args, **kwargs):
        query = request.query_params.get('query', None)
        if query:
            queryset = Post.objects.filter(
                Q(tag__name__icontains=query)
            ).distinct().order_by('-created_at')
        else:
            queryset = Post.objects.all().order_by('-created_at')

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class TagViewSet(viewsets.ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer

    def get_permissions(self):
        if self.action == 'list':
            permission_classes = [AllowAny]
        elif self.action in ['update', 'partial_update', 'destroy', 'create']:
            permission_classes = [IsAdminUser]
        else:
            permission_classes = [AllowAny]
        return [permission() for permission in permission_classes]


class CommentsViewSet(viewsets.ModelViewSet):
    queryset = Comments.objects.all()
    serializer_class = CommentPostSerializer

    def get_permissions(self):
        if self.action in ['list', 'retrieve']:
            permission_classes = [AllowAny]
        elif self.action in ['update', 'partial_update', 'destroy', 'create']:
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAuthenticated]
        return [permission() for permission in permission_classes]

    def list(self, request, *args, **kwargs):
        post_id = request.query_params.get('post_id')
        if not post_id:
            return Response({"error": "post_id is required"}, status=400)

        comments = Comments.objects.filter(post_id=post_id).order_by('-created_at')
        serializer = CommentPostSerializer(comments, many=True)

        if not serializer.data:
            return Response([], status=200)
        return Response(serializer.data, status=200)


class PhotoToPostView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def get_permissions(self):
        if self.request.method == 'GET':
            permission_classes = [AllowAny]
        elif self.request.method == 'POST':
            permission_classes = [IsAdminUser]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

    def get(self, request, *args, **kwargs):
        try:
            photos = Photo.objects.filter(post_id=kwargs['post_id'])
            if not photos.exists():
                return Response(status=404, data={'message': 'No photos found for this post id'})
            serializer = PhotoPostSerializer(photos, many=True, context={'request': request})
            return Response(serializer.data, status=200)
        except Photo.DoesNotExist:
            return Response(status=404, data={'message': 'No such photo by post id'})

    def post(self, request, *args, **kwargs):
        try:
            print(request.data)
            photo, created = Photo.objects.get_or_create(post_id=kwargs['post_id'])
            serializer = PhotoPostSerializer(photo, data=request.data, partial=True, context={'request': request})
            if serializer.is_valid():
                if 'image_write' in request.FILES:
                    photo.image = request.FILES['image_write']
                    photo.save()
                else:
                    serializer.save()
                return Response(serializer.data, status=200)
            else:
                return Response(serializer.errors, status=400)
        except Photo.DoesNotExist:
            return Response(status=404, data={'message': 'No such photo by post id'})
