from rest_framework import serializers

from accounts.serializers import UserSerializer
from article.models import Post, Tag, Photo, Comments


class PostSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format='%Y-%m-%d', read_only=True)
    updated_at = serializers.DateTimeField(format='%Y-%m-%d', read_only=True)

    class Meta:
        model = Post
        fields = '__all__'


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class PhotoPostSerializer(serializers.ModelSerializer):
    image_write = serializers.ImageField(write_only=True)
    image = serializers.SerializerMethodField()
    post = serializers.PrimaryKeyRelatedField(queryset=Post.objects.all())

    class Meta:
        model = Photo
        fields = ['id', 'post', 'image', 'image_write']

    def get_image(self, photo):
        if photo.image:
            request = self.context.get('request')
            image_url = photo.image.url
            return request.build_absolute_uri(image_url)
        return None

    def update(self, instance, validated_data):
        if 'image_write' in validated_data:
            instance.image = validated_data.pop('image_write')
        return super().update(instance, validated_data)

    def create(self, validated_data):
        if 'image_write' in validated_data:
            validated_data['image'] = validated_data.pop('image_write')
        return super().create(validated_data)


class CommentPostSerializer(serializers.ModelSerializer):
    created_at = serializers.DateTimeField(format='%Y-%m-%d: %H-%m', read_only=True)
    user_info = UserSerializer(source='user', read_only=True)

    class Meta:
        model = Comments
        fields = ['id', 'post', 'user', 'text', 'created_at', 'user_info']
