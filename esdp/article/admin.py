from django.contrib import admin
from article import models


@admin.register(models.Tag)
class Tag(admin.ModelAdmin):
    list_display = ('id', 'name')


admin.site.register(models.Post)
admin.site.register(models.Comments)
