from django.urls import path, include
from rest_framework.routers import DefaultRouter
from article import views

router = DefaultRouter()
router.register(r'post', views.PostViewSet)
router.register(r'tag', views.TagViewSet)
router.register(r'comment', views.CommentsViewSet)

urlpatterns = [
    path('get-posts-photo/<uuid:post_id>', views.PhotoToPostView.as_view(), name='get-posts-photo')
]
