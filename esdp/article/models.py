import uuid

from django.contrib.auth import get_user_model
from django.db import models


class Tag(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    name = models.CharField(max_length=50, verbose_name='Тег')

    def __str__(self):
        return self.name


class Post(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    user = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name='posts', verbose_name='Пользователь'
    )
    title = models.CharField(max_length=100, null=False, blank=False, verbose_name='Название')
    text = models.TextField(max_length=100000, null=False, blank=False, verbose_name='Описание')
    tag = models.ManyToManyField(Tag, related_name='tags', verbose_name='tags')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата обновления')

    def __str__(self):
        return self.title


class Photo(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='photos', verbose_name='post_id')
    image = models.ImageField(upload_to='article/', blank=False, null=False)

    def __str__(self):
        return self.id


class Comments(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    post = models.ForeignKey(Post, related_name='comments', on_delete=models.CASCADE, verbose_name='Статья')
    user = models.ForeignKey(
        get_user_model(), on_delete=models.CASCADE, related_name='comments', verbose_name='Пользователь'
    )
    text = models.TextField(null=False, blank=False, verbose_name='Описание')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')

    def __str__(self):
        return self.id
