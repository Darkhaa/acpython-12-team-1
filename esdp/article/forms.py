from django import forms
from .models import Post, Comments
from django.forms import widgets


class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ('title', 'text', 'tag')
        widgets = {
            'title': widgets.TextInput(attrs={
                'class': 'form-control mb-3',
            }),
            'text': widgets.Textarea(attrs={
                'class': 'form-control mb-3',
            }),
            'tag': widgets.RadioSelect
        }


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comments
        fields = ('text',)
