from rest_framework.views import APIView
from django.middleware import csrf
from django.http import JsonResponse


class GetCsrfToken(APIView):
    """
    генерация csrfToken для того что бы использовать его на фронте
    """
    def get(self, request, *args, **kwargs):
        csrf_token = csrf.get_token(request)
        return JsonResponse({'csrf_token': csrf_token})
