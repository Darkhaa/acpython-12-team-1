from django.urls import path, include
from accounts.urls import router as account_router
from main_tree.urls import router as main_tree_router
from article.urls import router as article_router
from api import views
from rest_framework.authtoken.views import obtain_auth_token


urlpatterns = [
    # Полезные штуки
    path('login/', obtain_auth_token, name='login-api'),
    path('get-csrftoken/', views.GetCsrfToken.as_view(), name='get-csrftoken'),
    # Посты
    path('article/', include('article.urls')),
    path('article/', include(article_router.urls)),

    # main tree
    path('shezire/', include('main_tree.urls')),
    path('shezire/', include(main_tree_router.urls)),

    # family tree
    path('family-tree/', include('family_tree.urls')),

    # Аккаунты
    path('accounts/', include(account_router.urls)),
    path('accounts/', include('accounts.urls')),
]
