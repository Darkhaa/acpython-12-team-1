from django.contrib.auth import get_user_model
from rest_framework import serializers
from accounts.models import Profile
from rest_framework.authtoken.models import Token
from accounts.models import User


class UserSerializerForProfile(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'email', 'phone']
        read_only_fields = ['id', ]


class IsStaffSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'is_staff']
        read_only_fields = ['id']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ['id', 'email', 'phone', 'is_admin']
        read_only_fields = ['id', ]


class ProfileSerializer(serializers.ModelSerializer):
    user_id = serializers.PrimaryKeyRelatedField(source='user', queryset=User.objects.all())
    avatar = serializers.SerializerMethodField()
    avatar_write = serializers.ImageField(write_only=True)

    class Meta:
        model = Profile
        fields = ['id', 'user_id', 'zhuz', 'clan', 'house', 'avatar', 'avatar_write']
        read_only_fields = ['id', 'user_id']

    def get_avatar(self, profile):
        if profile.avatar:
            request = self.context.get('request')
            avatar_url = profile.avatar.url
            return request.build_absolute_uri(avatar_url)
        else:
            return None


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField()
    new_password = serializers.CharField()
    confirm_password = serializers.CharField()

    def validate(self, data):
        new_password = data.get('new_password')
        confirm_password = data.get('confirm_password')

        if new_password != confirm_password:
            raise serializers.ValidationError("New password and confirm password do not match.")

        return data


class SurveySerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ['id', 'zhuz', 'clan', 'house']
        read_only_fields = ['id', ]


class GetCurrentUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = Token
        fields = ['user']


class EmailSerializer(serializers.Serializer):
    email = serializers.EmailField()


class ResetPasswordSerializer(serializers.Serializer):
    reset_password_token = serializers.CharField(required=True)
    new_password = serializers.CharField(write_only=True, required=True)
