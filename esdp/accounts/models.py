import uuid
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from main_tree.models import Zhuz, Clan, House, Ancestor


class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    phone = models.CharField(max_length=12, blank=False, null=False)
    email = models.EmailField(blank=False, null=False, unique=True)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(
        _("active"),
        default=False,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )

    def save(self, *args, **kwargs):
        self.username = self.email
        super().save(*args, **kwargs)


class Profile(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    user = models.OneToOneField(get_user_model(), on_delete=models.PROTECT)
    is_private_tree = models.BooleanField(default=True)
    avatar = models.ImageField(upload_to='avatar/', blank=True, null=True, default='avatar/default.jpg')
    zhuz = models.ForeignKey(Zhuz, on_delete=models.PROTECT, blank=True, null=True)
    clan = models.ForeignKey(Clan, on_delete=models.PROTECT, blank=True, null=True)
    house = models.ForeignKey(House, on_delete=models.PROTECT, blank=True, null=True)
    survey = models.JSONField(default=dict, blank=True, null=True, encoder=DjangoJSONEncoder)
    is_subscribed = models.BooleanField(default=False)


class Shejire(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4)
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    house = models.ManyToManyField(House, blank=True)
    ancestor = models.ManyToManyField(Ancestor, blank=True)
    is_approved = models.BooleanField(default=True)
    reason = models.CharField(max_length=150, blank=True, null=False, default='Админ отказал вам в верификации Шежiре')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата обновления')