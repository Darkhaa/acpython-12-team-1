from django.conf import settings
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.core.cache import cache
from rest_framework import status
from rest_framework.exceptions import PermissionDenied, NotFound
from rest_framework.permissions import IsAuthenticated, AllowAny

from accounts.serializers import ChangePasswordSerializer, EmailSerializer, ResetPasswordSerializer
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import get_user_model
import logging
from accounts.utils.utils import send_reset_password_mail

logger = logging.getLogger(__name__)


class ChangePasswordView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        """
        param: old_password: <PASSWORD>
        param: new_password: <PASSWORD>
        param: new_password2: <PASSWORD>
        return: status
        """
        try:
            user = get_user_model().objects.get(id=kwargs['user_id'])
            if request.user.id != user.id:
                raise PermissionDenied("You do not have permission to access this profile.")

            serializer = ChangePasswordSerializer(user, data=request.data)

            if serializer.is_valid():
                if not user.check_password(serializer.validated_data.get('old_password')):
                    return Response({"detail": "Old password is incorrect."}, status=status.HTTP_400_BAD_REQUEST)

                user.set_password(serializer.validated_data.get('new_password'))
                user.save()

                return Response({"detail": "Password changed successfully."}, status=status.HTTP_200_OK)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        except get_user_model().DoesNotExist:
            raise NotFound("User not found")
        except PermissionDenied as e:
            return Response({'error': str(e)}, status=status.HTTP_403_FORBIDDEN)


class ResetPasswordSendEmailTokenView(APIView):
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        """
        param: email
        return email_post
        """
        serializer = EmailSerializer(data=request.data)
        if serializer.is_valid():
            email = serializer.validated_data['email']

            try:
                user = get_user_model().objects.get(email=email)
                send_reset_password_mail(user)
                return Response(status=status.HTTP_200_OK, data={'message': 'Reset password mail sent successfully'})
            except get_user_model().DoesNotExist:
                return Response(status=status.HTTP_400_BAD_REQUEST,
                                data={'error': 'User with this email does not exist'})
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ResetPasswordView(APIView):
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        serializer = ResetPasswordSerializer(data=request.data)
        if serializer.is_valid():
            token = serializer.validated_data['reset_password_token']
            redis_key = settings.TAYPA_USER_RESET_PASSWORD_KEY.format(token=token)
            user_info = cache.get(redis_key) or {}

            if user_id := user_info.get('user_id'):
                try:
                    user = get_user_model().objects.get(id=user_id)
                    if user is not None and PasswordResetTokenGenerator().check_token(user, user_info.get('reset_token')):
                        user.set_password(serializer.validated_data['new_password'])
                        user.save()
                        cache.delete(redis_key)  # Optionally clear the cache after successful reset
                        return Response(status=status.HTTP_200_OK, data={'message': 'Password has been reset successfully'})
                    else:
                        logger.error('Invalid or expired token for user_id %s', user_id)
                        return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': 'Token is invalid or expired'})
                except get_user_model().DoesNotExist:
                    logger.error('User not found with user_id %s', user_id)
                    return Response({'detail': 'User not found'}, status=status.HTTP_404_NOT_FOUND)
            else:
                logger.error('Invalid token: %s', token)
                return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': 'Invalid token'})
        else:
            logger.error('Serializer validation failed: %s', serializer.errors)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
