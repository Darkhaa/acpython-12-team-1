from accounts.serializers import GetCurrentUserSerializer
from rest_framework.exceptions import NotFound
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.models import Token


class GetUserByAuthToken(APIView):
    def get(self, request, *args, **kwargs):
        """
        param: auth_token
        return: user id по токену который находится в local storage
        """
        try:
            token = kwargs['auth_token']
            auth_token = Token.objects.get(key=token)
            serializer = GetCurrentUserSerializer(auth_token)
            return Response(serializer.data)
        except NotFound:
            raise NotFound("Not Found")
