from rest_framework import status, viewsets
from accounts.serializers import UserSerializer
from accounts.utils.utils import send_mail_after_reg
from django.core.exceptions import ValidationError
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response
from django.contrib.auth import get_user_model


class UserViewSet(viewsets.ModelViewSet):
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer

    def create(self, request, *args, **kwargs):
        """
        param: email
        param: phone
        return: user.object и отправка сообщения на почту, если все окей
        """
        try:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)

            user = serializer.instance
            if user:
                send_mail_after_reg(user)

            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

        except ValidationError as e:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=e.messages)