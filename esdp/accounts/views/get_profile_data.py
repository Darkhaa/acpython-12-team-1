from rest_framework import status
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated

from accounts.models import Profile
from accounts.serializers import ProfileSerializer, UserSerializerForProfile, SurveySerializer
from family_tree.models import FamilyTree
from rest_framework.exceptions import NotFound, PermissionDenied
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from django.contrib.auth import get_user_model


class GetUserProfileDataView(APIView):
    permission_classes = [IsAuthenticated]

    def get_permissions(self):
        if self.request.method in ['GET', 'PATCH']:
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

    def get(self, request, *args, **kwargs):
        """
        param: user_id
        Получение user object по user_id
        """
        try:
            user = get_user_model().objects.get(id=kwargs['user_id'])
            if request.user.id != user.id:
                raise PermissionDenied("You do not have permission to access this profile.")

            profile, _ = Profile.objects.get_or_create(user_id=user.id)
            user_profile = ProfileSerializer(profile, context={'request': request})
            serializer = UserSerializerForProfile(user)
            return Response({'user': serializer.data, 'profile': user_profile.data})
        except get_user_model().DoesNotExist:
            raise NotFound("User not found")
        except PermissionDenied as e:
            return Response({'error': str(e)}, status=status.HTTP_403_FORBIDDEN)

    def patch(self, request, *args, **kwargs):
        """
        param: user_id
        изменение данных такие как 'email', 'phone'
        """
        try:
            user = get_user_model().objects.get(id=kwargs['user_id'])
            if request.user.id != user.id:
                raise PermissionDenied("You do not have permission to access this profile.")

            serializer = UserSerializerForProfile(user, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=400)
        except get_user_model().DoesNotExist:
            raise NotFound("User not found")
        except PermissionDenied as e:
            return Response({'error': str(e)}, status=status.HTTP_403_FORBIDDEN)


class UpdatePhoto(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def get_permissions(self):
        if self.request.method == 'PATCH':
            permission_classes = [IsAuthenticated]
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]

    def patch(self, request, *args, **kwargs):
        try:
            user = get_user_model().objects.get(id=kwargs['user_id'])
            if request.user.id != user.id:
                raise PermissionDenied("You do not have permission to access this profile.")

            profile = Profile.objects.get(user_id=user.id)
            serializer = ProfileSerializer(profile, data=request.data, partial=True, context={'request': request})
            if serializer.is_valid():
                if 'avatar_write' in request.data:
                    profile.avatar = request.data['avatar_write']
                    profile.save()
                serializer.save()
                return Response(serializer.data, status=200)
            else:
                return Response(serializer.errors, status=400)
        except get_user_model().DoesNotExist:
            raise NotFound("User not found")
        except Profile.DoesNotExist:
            raise NotFound("Profile not found")
        except PermissionDenied as e:
            return Response({'error': str(e)}, status=status.HTTP_403_FORBIDDEN)
