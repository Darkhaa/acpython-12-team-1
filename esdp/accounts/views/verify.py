from django.core.cache import cache
from rest_framework import status
from accounts.utils.utils import get_auth_token, get_auth_data
from django.conf import settings
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import get_user_model


class RegisterConfirm(APIView):
    def post(self, request, *args, **kwargs):
        """
        param: verify_token: uuid
        Передаётся хэш, который содержит информацию о user.id, user.email, user.password.
        Хэш хранится всего 30-40 минут, можно сделать меньше.
        Метод post для более безопасной передачи хэша.
        Возвращается auth_token, который будет помещён в localStorage.
        И сразу будет перенаправление в настройки пользователя для заполнения информации (смены пароля) на фронте.
        """

        redis_key = settings.TAYPA_USER_CONFIRMATION_KEY.format(token=request.data.get('verify_token'))

        user_info = cache.get(redis_key) or {}
        if user_id := user_info.get('user_id'):

            try:
                user = get_user_model().objects.get(id=user_id)
            except get_user_model().DoesNotExist:
                return Response({'detail': 'User not found'}, status=status.HTTP_404_NOT_FOUND)

            auth_data = get_auth_data(user, user_info)
            auth_token = get_auth_token(auth_data)

            if auth_token:
                return Response({'Auth_token': auth_token}, status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'Unable to obtain auth token'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response({'detail': 'User ID not found in verification token'}, status=status.HTTP_400_BAD_REQUEST)
