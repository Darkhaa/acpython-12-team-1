from django.contrib.auth import get_user_model
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from accounts.serializers import IsStaffSerializer


class IsStaffUserView(APIView):
    permission_classes = [AllowAny]

    def get(self, request, *args, **kwargs):
        user = get_user_model().objects.get(id=kwargs.get('user_id'))
        serializer = IsStaffSerializer(user)
        if serializer.data['is_staff'] is True:
            return Response({'is_staff': True})
        else:
            return Response({'is_staff': False})
