from .logout import LogoutView
from .get_user_by_auth_token import GetUserByAuthToken
from .verify import RegisterConfirm
from .register_user import UserViewSet
from .password import ChangePasswordView, ResetPasswordSendEmailTokenView, ResetPasswordView
from .get_profile_data import GetUserProfileDataView, UpdatePhoto
from .get_staff import IsStaffUserView
