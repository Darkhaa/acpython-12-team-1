import uuid

from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.core.cache import cache
from django.core.mail import send_mail
from django.test import RequestFactory
from rest_framework.authtoken.views import ObtainAuthToken
from accounts.models import User
from core.settings import HOST_FRONT


def _random_password():
    return get_user_model().objects.make_random_password()


def get_auth_token(auth_data):
    factory = RequestFactory()
    auth_request = factory.post('/api/token-auth/', auth_data, format='json')
    auth_view = ObtainAuthToken.as_view()
    response = auth_view(auth_request)
    auth_token = response.data.get('token')
    return auth_token


def get_auth_data(user: User, user_info):
    user.is_active = True
    user.save(update_fields=['is_active'])

    return {
        'username': user_info.get('email'),
        'password': user_info.get('password')
    }


def send_mail_after_reg(user: User):
    password = _random_password()
    user.set_password(password)
    user.save()

    token = uuid.uuid4().hex
    redis_key = settings.TAYPA_USER_CONFIRMATION_KEY.format(token=token)
    cache.set(redis_key, {"user_id": user.id, "email": user.email, "password": password},
              timeout=settings.TAYPA_USER_CONFIRMATION_TIMEOUT)

    subject = 'Taypa регистрация'
    message = (f'Спасибо за регистрацию! Ваш временный пароль: ({password}) \nПожалуйста, '
               f'перейдите по ссылке, чтобы завершить процесс регистрации http://{HOST_FRONT}/verify/{token}')

    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)


def send_reset_password_mail(user: User):
    reset_password_token = PasswordResetTokenGenerator().make_token(user)

    token = uuid.uuid4().hex
    redis_key = settings.TAYPA_USER_RESET_PASSWORD_KEY.format(token=token)
    cache.set(redis_key, {'user_id': user.id, "reset_token": reset_password_token},
              timeout=settings.TAYPA_USER_RESET_PASSWORD_TIMEOUT)

    subject = 'Taypa восстановление пароля'
    message = f'Перейдите по ссылки http://{HOST_FRONT}/reset-password/{token}'

    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [user.email], fail_silently=False)
