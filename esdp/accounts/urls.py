from django.urls import path
from accounts import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)

urlpatterns = [
    path('get_user/<uuid:user_id>', views.GetUserProfileDataView.as_view(), name='get_user_by_email'),
    path('password_change/<uuid:user_id>', views.ChangePasswordView.as_view(), name='change_password'),
    path('verify', views.RegisterConfirm.as_view(), name='verify_user'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('get_user_auth_token/<str:auth_token>', views.GetUserByAuthToken.as_view(), name='get_user'),
    path('send-mail-reset_password/', views.ResetPasswordSendEmailTokenView.as_view(), name='send-reset-password-token'),
    path('reset_password/', views.ResetPasswordView.as_view(), name='reset-password'),
    path('update-photo/<uuid:user_id>', views.UpdatePhoto.as_view(), name='update-photo'),
    path('is_staff_user/<uuid:user_id>', views.IsStaffUserView.as_view(), name='is_staff_user')

]
