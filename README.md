# Запуск проекта 

## Backend:
создаем виртуальное окружение, устанавливаем зависимости
```sh
pip3 install -r reqs.txt
cd esdp
python manage.py migrate 
```
## Oткрываем второй терминал 

## Frontend:
```node.js > 16 version```
если не установлен node.js устанавливаем с ссылки [Node.js](https://blog.sedicomm.com/2022/12/14/install-nodejs-npm-in-centos-ubuntu/#2)

далее пишем команду
```sh
npm install vite --save-dev
npm run dev
```

## Redis

Установка Redis для Ubuntu
[Download and setting Redis](https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-20-04-ru)

```sh 
sudo apt install redis-server
```
Откройте конфигурационный файл:
```sh 
sudo nano /etc/redis/redis.conf
```
Внутри файла найдите директиву supervised. Эта директива позволяет объявить систему инициализации для управления Redis как службой, предоставляя вам более широкий контроль за ее работой. Для директивы supervised по умолчанию установлено значение no. Поскольку вы запускаете Ubuntu, которая использует систему инициализации systemd, измените значение на systemd:
```
. . .

# If you run Redis from upstart or systemd, Redis can interact with your
# supervision tree. Options:
#   supervised no      - no supervision interaction
#   supervised upstart - signal upstart by putting Redis into SIGSTOP mode
#   supervised systemd - signal systemd by writing READY=1 to $NOTIFY_SOCKET
#   supervised auto    - detect upstart or systemd method based on
#                        UPSTART_JOB or NOTIFY_SOCKET environment variables
# Note: these supervision methods only signal "process is ready."
#       They do not enable continuous liveness pings back to your supervisor.
supervised systemd

. . .
```
Это единственное изменение, которое вам нужно внести в файл конфигурации Redis на данный момент,поэтому сохраните и закройте его после завершения редактирования. Если вы использовали nano для редактирования файла, нажмите CTRL + X, Y, а затем ENTER.

Перезапустите службу Redis, чтобы изменения в файле конфигурации вступили в силу:
```sh
sudo systemctl restart redis.service
```

Установка Redis на Windows [Github](https://github.com/tporadowski/redis/releases),
устанавливаем ```Redis-x64-5.0.14.1.zip```.
Запускаем ```redis-server.exe и redis-cli.exe```.

## Проект запущен, поздравляю 
