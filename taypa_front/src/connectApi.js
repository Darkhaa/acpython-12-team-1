import axios from "axios";
import router from "@/router.js";

export let baseUser = '',
    APIURL = `/api`,
    ACCOUNT = `${APIURL}/accounts`,
    FAMILYTREE = `${APIURL}/family-tree`,
    SHEZIRE =`${APIURL}/shezire`,
    ARTICLE = `${APIURL}/article`;


export function getAuthToken() {
    return 'Token ' + localStorage.getItem('apiToken');
}

export function getCsrfToken(){
     return axios.get(`${APIURL}/get-csrftoken/`);
}

export function getOnlyAuthToken() {
    return localStorage.getItem('apiToken');
}


export async function getCurrentUserId(){
    let authToken = getOnlyAuthToken();
    let headers = {
      'Content-Type': 'application/json',
        'Authorization': authToken
    };

    const response = await axios.get(`${ACCOUNT}/get_user_auth_token/${authToken}`, {headers: headers});
    return response.data.user;
}

export async function getCurrentUserData(username){
    try {
        let user_id = await getCurrentUserId();
        let authToken = getAuthToken(),
        headers = {
        'Content-Type': 'application/json',
        'Authorization': authToken
        }
        const resp = await axios.get(`${ACCOUNT}/get_user/${user_id}`, {headers: headers});
        username.value = resp.data.user.email;
    } catch (error) {
        console.log(error);
    }
}

export function logout(){
    let csrf = getCsrfToken(),
        authToken = getAuthToken(),
        headers = {
        'Content-Type': 'application/json',
            'X-CSRFToken': csrf,
            'Authorization': authToken
        }
    axios.post(`${ACCOUNT}/logout/`,{},{headers: headers}).then(response =>{
        localStorage.removeItem('apiToken');
        router.push('/').then(() => window.location.reload());
    }).catch(error => {
        console.log(error);
    })
}

export function getIdFromUrl(){
    let url = new URL(window.location.href),
    path = url.pathname.split('/').filter(Boolean);
    return path[path.length - 1]
}

export function loadZhuzs() {
  return axios.get(`${SHEZIRE}/zhuzs/`).then(response => response.data)
    .catch(error => {
      console.error('Ошибка при загрузке жузов:', error);
      return [];
    });
}

export async function isStaffUser(){
     try {
        const userId = await getCurrentUserId();
        const response = await axios.get(`${ACCOUNT}/is_staff_user/${userId}`);
        return response.data.is_staff === true;
    } catch (error) {
        console.error('Error checking if user is staff:', error);
        return false;
    }
}