import { createRouter, createWebHistory } from 'vue-router';
import Home from './views/indexPage.vue';
import Login from './views/RegLogRes/loginPage.vue';
import Profile from './views/profile/ProfilePage.vue';
import Setting from './views/profile/SettingPage.vue';
import Tree from './views/treePage.vue';
import Registration from './views/RegLogRes/registrationPage.vue';
import MyTreePage from "@/views/MyTreePage.vue";
import Verify from "./views/Verify.vue";
import ChangePassword from "./views/profile/ChangePass.vue";
import ResetPassword from "./views/RegLogRes/ResetPassword.vue"
import ResetPasswordConfirm from "./views/RegLogRes/ResetPasswordConfirm.vue"
import Edit from "./views/article/PostEdit.vue"
import PostList from "./views/article/PostList.vue"
import PostDetail from "./views/article/PostDetail.vue"
import Index from '@/views/index/Index.vue'
import Admin from "@/views/admin.vue";
import Request from "@/views/admin_requests.vue";

const routes = [
  { path: '/index', component: Home },
  { path: '/login', component: Login },
  { path: '/profile/:uuid', component: Profile },
  { path: '/profile/setting/:uuid', component: Setting },
  { path: '/registration', component: Registration },
  { path: '/tree', component: Tree },
  { path: '/mytree/:uuid', component: MyTreePage },
  { path: '/verify/:uuid', component: Verify },
  { path: '/change-password/:uuid', component: ChangePassword },
  { path: '/reset-password', component: ResetPassword },
  { path: '/reset-password/:uuid', component: ResetPasswordConfirm },
  { path: '/edit', component: Edit },
  { path: '/edit/:postId', component: Edit },
  { path: '/posts', component: PostList },
  { path: '/post/:postId', component: PostDetail },
  { path: '/admin', component: Admin },
  { path: '/admin/requests', component: Request },
  { path: '/', component: Index }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;
