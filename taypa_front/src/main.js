import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import './assets/main.css';
import '../node_modules/flowbite-vue/dist/index.css';
import VueBlocksTree from 'vue3-blocks-tree';
import 'vue3-blocks-tree/dist/vue3-blocks-tree.css';
import i18n from './i18n';
import axios from "axios";

let defaultOptions = { treeName: 'blocks-tree' };

const app = createApp(App);

axios.defaults.baseURL = `http://${import.meta.env.VITE_BACKEND_HOST}:${import.meta.env.VITE_BACKEND_PORT}`;

app.use(router);
app.use(VueBlocksTree, defaultOptions);
app.use(i18n);
app.mount('#app');