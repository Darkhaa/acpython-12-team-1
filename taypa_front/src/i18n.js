import { createI18n } from 'vue-i18n';
import en from './locales/en.json';
import ru from './locales/ru.json';
import kz from './locales/kz.json'


const messages = {
  en,
  ru,
  kz
};

// Получить язык из localStorage или юзаем дефолтный
const savedLocale = localStorage.getItem('locale') || 'ru'; //устанавливать дефолтный язык в этой строчке

const i18n = createI18n({
  legacy: false,
  locale: savedLocale, // Язык по дефолту
  messages,
});

export default i18n;
