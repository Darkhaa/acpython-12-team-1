import {ref} from "vue";

export const globalData = {
    hightZhuzNumber: ref(800),
  middleZhuzNumber: ref(550),
  youngZhuzNumber: ref(700),
  otherItemNumber: ref(350)
};